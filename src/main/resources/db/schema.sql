DROP TABLE IF EXISTS BLPU, DtfLine, LPI, RecordIdentifier, StreetDescriptor, StreetRecord, Version;
DROP VIEW IF EXISTS vwAddresses;
DROP VIEW IF EXISTS  vwdtfline;
DROP VIEW IF EXISTS  vwlpi;

DROP PROCEDURE IF EXISTS  spTruncateEverything;

DROP PROCEDURE IF EXISTS spInsertIntoBLPU;
DROP PROCEDURE IF EXISTS spInsertIntoDtfLine;
DROP PROCEDURE IF EXISTS spInsertIntoLPI;
DROP PROCEDURE IF EXISTS spInsertIntoRecordIdentifier;
DROP PROCEDURE IF EXISTS spInsertIntoStreetDescriptor;
DROP PROCEDURE IF EXISTS spInsertIntoStreetRecord;
DROP PROCEDURE IF EXISTS spInsertIntoRecordVersion;

CREATE TABLE RecordIdentifier (
    RecordIdentifierId INT NOT NULL,
    Description LONGTEXT NULL,
    CONSTRAINT PK_RecordType PRIMARY KEY (RecordIdentifierId ASC)
);


CREATE TABLE LPI(
	UPRN bigint NULL,
	LPI_KEY Longtext NULL,
	LANGUAGE Longtext NULL,
	LOGICAL_STATUS bigint NULL,
	START_DATE date NULL,
	END_DATE date NULL,
	ENTRY_DATE date NULL,
	LAST_UPDATE_DATE date NULL,
	SAO_START_NUMBER bigint NULL,
	SAO_START_SUFFIX Longtext NULL,
	SAO_END_NUMBER bigint NULL,
	SAO_END_SUFFIX Longtext NULL,
	SAO_TEXT Longtext NULL,
	PAO_START_NUMBER bigint NULL,
	PAO_START_SUFFIX Longtext NULL,
	PAO_END_NUMBER bigint NULL,
	PAO_END_SUFFIX Longtext NULL,
	PAO_TEXT Longtext NULL,
	USRN bigint NULL,
	LEVEL Longtext NULL,
	POSTAL_ADDRESS Longtext NULL,
	POSTCODE Longtext NULL,
	POST_TOWN Longtext NULL,
	OFFICIAL_FLAG Longtext NULL,
	CUSTODIAN_ONE bigint NULL,
	CUSTODIAN_TWO bigint NULL,
	CAN_KEY Longtext NULL,
	VersionId int NOT NULL,
	EntityId int AUTO_INCREMENT NOT NULL,
	CONSTRAINT PK_LPI PRIMARY KEY
(
	EntityId ASC
)
);
CREATE INDEX IX_LPI_1 ON LPI (UPRN ASC);
CREATE INDEX IX_LPI_2 ON LPI (USRN ASC);


CREATE TABLE DtfLine(
	RecordIdentifier int NULL,
	ChangeType Longtext NULL,
	ProOrder int NULL,
	TextLine Longtext NULL,
	VersionId int NULL,
	FieldCount int NULL,
	EntityId int AUTO_INCREMENT NOT NULL,
	CONSTRAINT PKPRIMARYPRIMARY_Line PRIMARY KEY
(
	EntityId ASC
)
);
CREATE INDEX IX_DtfLine_1 ON DtfLine (EntityId ASC);


CREATE TABLE BLPU(
	UPRN bigint NULL,
	LOGICAL_STATUS bigint NULL,
	BLPU_STATE bigint NULL,
	BLPU_STATE_DATE date NULL,
	BLPU_CLASS Longtext NULL,
	PARENT_UPRN bigint NULL,
	X_COORDINATE decimal(12, 2) NULL,
	Y_COORDINATE decimal(12, 2) NULL,
	RPC bigint NULL,
	LOCAL_CUSTODIAN_CODE bigint NULL,
	START_DATE date NULL,
	END_DATE date NULL,
	LAST_UPDATE_DATE date NULL,
	ENTRY_DATE date NULL,
	ORGANISATION Longtext NULL,
	WARD_CODE Longtext NULL,
	PARISH_CODE Longtext NULL,
	CUSTODIAN_ONE bigint NULL,
	CUSTODIAN_TWO bigint NULL,
	CAN_KEY Longtext NULL,
	VersionId int NOT NULL,
	EntityId int AUTO_INCREMENT NOT NULL,
 CONSTRAINT PK_BLPU PRIMARY KEY
(
	EntityId ASC
)
);
CREATE INDEX IX_BLPU_1 ON BLPU (UPRN ASC);

CREATE TABLE Version(
	VersionId int AUTO_INCREMENT NOT NULL,
	StartDateTime datetime(3) NOT NULL,
	EndDateTime datetime(3) NULL,
	SourceFilename Longtext NOT NULL,
	Messages Longtext NULL,
	CONSTRAINT PK_Version PRIMARY KEY
(
	VersionId ASC
)
);


CREATE TABLE StreetRecord(
	USRN bigint NULL,
	RECORD_TYPE bigint NULL,
	SWA_ORG_REF_NAMING bigint NULL,
	STATE bigint NULL,
	STATE_DATE date NULL,
	STREET_SURFACE bigint NULL,
	STREET_CLASSIFICATION bigint NULL,
	VERSION bigint NULL,
	RECORD_ENTRY_DATE date NULL,
	LAST_UPDATE_DATE date NULL,
	STREET_START_DATE date NULL,
	STREET_END_DATE date NULL,
	STREET_START_X decimal(12, 2) NULL,
	STREET_START_Y decimal(12, 2) NULL,
	STREET_END_X decimal(12, 2) NULL,
	STREET_END_Y decimal(12, 2) NULL,
	STREET_TOLERANCE bigint NULL,
	VersionId int NOT NULL,
	EntityId int AUTO_INCREMENT NOT NULL,
	CONSTRAINT PK_StreetRecord PRIMARY KEY
(
	EntityId ASC
)
);


CREATE TABLE StreetDescriptor(
	USRN bigint NULL,
	STREET_DESCRIPTOR Longtext NULL,
	LOCALITY_NAME Longtext NULL,
	TOWN_NAME Longtext NULL,
	ADMINSTRATIVE_AREA Longtext NULL,
	LANGUAGE Longtext NULL,
	VersionId int NOT NULL,
	EntityId int AUTO_INCREMENT NOT NULL,
	CONSTRAINT PK_StreetDescriptor PRIMARY KEY
(
	EntityId ASC
)
);

CREATE VIEW vwLpi
AS
SELECT LTRIM(IFNULL(SAO_TEXT, '')) AS SaoDesc,
			LTRIM(Concat(
				CASE WHEN SAO_START_NUMBER IS NULL THEN '' ELSE CONCAT(' ', SAO_START_NUMBER, ifnull(SAO_START_SUFFIX, '')) END,
        CASE WHEN SAO_END_NUMBER IS NULL THEN '' ELSE CONCAT('-', SAO_END_NUMBER, ifnull(SAO_END_SUFFIX, '')) END
			)) AS SaoNumber,
			LTRIM(IFNULL(PAO_TEXT, '')) AS PaoDesc,
			LTRIM(Concat(
				CASE WHEN PAO_START_NUMBER IS NULL THEN '' ELSE Concat(' ', PAO_START_NUMBER, ifnull(PAO_START_SUFFIX, '')) END,
				CASE WHEN PAO_END_NUMBER IS NULL THEN '' ELSE Concat('-', PAO_END_NUMBER, ifnull(PAO_END_SUFFIX, '')) END
			)) AS PaoNumber,
			UPRN, LPI_KEY, LOGICAL_STATUS, USRN, POSTCODE, POST_TOWN
FROM LPI;


CREATE VIEW vwDtfLine
AS
SELECT     DtfLine.RecordIdentifier, DtfLine.ChangeType,DtfLine.ProOrder, DtfLine.TextLine, DtfLine.FieldCount, DtfLine.EntityId, DtfLine.VersionId, RecordIdentifier.Description as RecordIdentifierDescription,
                      Version.StartDateTime AS LoadDate, Version.SourceFilename
FROM         DtfLine LEFT OUTER JOIN
                      RecordIdentifier ON DtfLine.RecordIdentifier = RecordIdentifier.RecordIdentifierId LEFT OUTER JOIN
                      Version ON DtfLine.VersionId = Version.VersionId;



CREATE VIEW vwAddresses
AS
SELECT     vwLpi.UPRN, vwLpi.SaoDesc, vwLpi.SaoNumber,
                      vwLpi.PaoDesc, vwLpi.PaoNumber, StreetDescriptor.STREET_DESCRIPTOR AS Street,
                      vwLpi.POST_TOWN AS PostTown, vwLpi.POSTCODE,
                      vwLpi.LPI_KEY, vwLpi.LOGICAL_STATUS AS LpiStatus, BLPU.LOGICAL_STATUS AS BlpuStatus,
                      BLPU.BLPU_CLASS AS BlpuClass, BLPU.PARENT_UPRN AS ParentUprn, BLPU.X_COORDINATE AS Easting,
                      BLPU.Y_COORDINATE AS Northing

                       ,CASE WHEN BLPU_Class = 'PS' THEN 5 ELSE 0 END + CASE WHEN BLPU_Class = 'P' THEN 4 ELSE 0 END + CASE WHEN BLPU_Class LIKE 'X%' THEN 3 ELSE 0 END
                       + CASE WHEN BLPU_Class LIKE 'C%' THEN 2 ELSE 0 END + CASE WHEN (BLPU_Class LIKE 'R0%' OR
                      BLPU_Class LIKE 'RD%' OR
                      BLPU_Class LIKE 'RH%' OR
                      BLPU_Class LIKE 'RI%') THEN 1 ELSE 0 END AS BlpuClassId, vwLpi.LOGICAL_STATUS AS StatusId


FROM         vwLpi LEFT OUTER JOIN
                      BLPU ON vwLpi.UPRN = BLPU.UPRN LEFT OUTER JOIN
                      StreetDescriptor ON vwLpi.USRN = StreetDescriptor.USRN;




DELIMITER $$
CREATE PROCEDURE spTruncateEverything()
BEGIN
	truncate table DtfLine;
	truncate table Version;
	truncate table StreetRecord;
	truncate table StreetDescriptor;
	truncate table BLPU;
	truncate table LPI;
END$$
DELIMITER ;


/*
BLPU
DtfLine
LPI
RecordIdentifier
StreetDescriptor
StreetRecord
Version
*/

DELIMITER $$
CREATE PROCEDURE spInsertIntoBLPU(
	IN UPRN bigint,
	IN LOGICAL_STATUS bigint,
	IN BLPU_STATE bigint,
	IN BLPU_STATE_DATE date ,
	IN BLPU_CLASS Longtext ,
	IN PARENT_UPRN bigint ,
	IN X_COORDINATE decimal(12, 2) ,
	IN Y_COORDINATE decimal(12, 2) ,
	IN RPC bigint ,
	IN LOCAL_CUSTODIAN_CODE bigint ,
	IN START_DATE date ,
	IN END_DATE date ,
	IN LAST_UPDATE_DATE date ,
	IN ENTRY_DATE date ,
	IN ORGANISATION Longtext ,
	IN WARD_CODE Longtext ,
	IN PARISH_CODE Longtext ,
	IN CUSTODIAN_ONE bigint ,
	IN CUSTODIAN_TWO bigint ,
	IN CAN_KEY Longtext ,
	IN VersionId int
	)
BEGIN
	insert into BLPU (
			UPRN,
			LOGICAL_STATUS,
			BLPU_STATE,
			BLPU_STATE_DATE,
			BLPU_CLASS,
			PARENT_UPRN,
			X_COORDINATE,
			Y_COORDINATE,
			RPC,
			LOCAL_CUSTODIAN_CODE,
			START_DATE,
			END_DATE,
			LAST_UPDATE_DATE,
			ENTRY_DATE,
			ORGANISATION,
			WARD_CODE,
			PARISH_CODE,
			CUSTODIAN_ONE,
			CUSTODIAN_TWO,
			CAN_KEY,
			VersionId
			)
    values (
			UPRN,
			LOGICAL_STATUS,
			BLPU_STATE,
			BLPU_STATE_DATE,
			BLPU_CLASS,
			PARENT_UPRN,
			X_COORDINATE,
			Y_COORDINATE,
			RPC,
			LOCAL_CUSTODIAN_CODE,
			START_DATE,
			END_DATE,
			LAST_UPDATE_DATE,
			ENTRY_DATE,
			ORGANISATION,
			WARD_CODE,
			PARISH_CODE,
			CUSTODIAN_ONE,
			CUSTODIAN_TWO,
			CAN_KEY,
			VersionId
		);
END$$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE spInsertIntoDtfLine(
	IN RecordIdentifier int,
	IN ChangeType Longtext,
	IN ProOrder int,
	IN TextLine Longtext,
	IN VersionId int,
	IN FieldCount int
	)
BEGIN
	insert into DtfLine
				(
				RecordIdentifier,
				ChangeType,
				ProOrder,
				TextLine,
				VersionId,
				FieldCount
				)
                values
                (
                RecordIdentifier,
				ChangeType,
				ProOrder,
				TextLine,
				VersionId,
				FieldCount
                );

END$$
DELIMITER ;



DELIMITER $$
CREATE PROCEDURE spInsertIntoLPI(
		IN UPRN bigint,
		IN LPI_KEY Longtext,
		IN LANGUAGE Longtext,
		IN LOGICAL_STATUS bigint,
		IN START_DATE date,
		IN END_DATE date,
		IN ENTRY_DATE date,
		IN LAST_UPDATE_DATE date,
		IN SAO_START_NUMBER bigint,
		IN SAO_START_SUFFIX Longtext,
		IN SAO_END_NUMBER bigint,
		IN SAO_END_SUFFIX Longtext,
		IN SAO_TEXT Longtext,
		IN PAO_START_NUMBER bigint,
		IN PAO_START_SUFFIX Longtext,
		IN PAO_END_NUMBER bigint,
		IN PAO_END_SUFFIX Longtext,
		IN PAO_TEXT Longtext,
		IN USRN bigint,
		IN LEVEL Longtext,
		IN POSTAL_ADDRESS Longtext,
		IN POSTCODE Longtext,
		IN POST_TOWN Longtext,
		IN OFFICIAL_FLAG Longtext,
		IN CUSTODIAN_ONE bigint,
		IN CUSTODIAN_TWO bigint,
		IN CAN_KEY Longtext,
		IN VersionId int
	)
BEGIN
	insert into LPI
				(
					UPRN,
					LPI_KEY,
					LANGUAGE,
					LOGICAL_STATUS,
					START_DATE,
					END_DATE,
					ENTRY_DATE,
					LAST_UPDATE_DATE,
					SAO_START_NUMBER,
					SAO_START_SUFFIX,
					SAO_END_NUMBER,
					SAO_END_SUFFIX,
					SAO_TEXT,
					PAO_START_NUMBER,
					PAO_START_SUFFIX,
					PAO_END_NUMBER,
					PAO_END_SUFFIX,
					PAO_TEXT,
					USRN,
					LEVEL,
					POSTAL_ADDRESS,
					POSTCODE,
					POST_TOWN,
					OFFICIAL_FLAG,
					CUSTODIAN_ONE,
					CUSTODIAN_TWO,
					CAN_KEY,
					VersionId
				)
                values
                (
	                UPRN,
					LPI_KEY,
					LANGUAGE,
					LOGICAL_STATUS,
					START_DATE,
					END_DATE,
					ENTRY_DATE,
					LAST_UPDATE_DATE,
					SAO_START_NUMBER,
					SAO_START_SUFFIX,
					SAO_END_NUMBER,
					SAO_END_SUFFIX,
					SAO_TEXT,
					PAO_START_NUMBER,
					PAO_START_SUFFIX,
					PAO_END_NUMBER,
					PAO_END_SUFFIX,
					PAO_TEXT,
					USRN,
					LEVEL,
					POSTAL_ADDRESS,
					POSTCODE,
					POST_TOWN,
					OFFICIAL_FLAG,
					CUSTODIAN_ONE,
					CUSTODIAN_TWO,
					CAN_KEY,
					VersionId
                );
END$$
DELIMITER ;




DELIMITER $$
CREATE PROCEDURE spInsertIntoRecordIdentifier(
	IN RecordIdentifierId INT,
    IN Description LONGTEXT
	)
BEGIN
	insert into RecordIdentifier
				(
					RecordIdentifierId,
					Description
				)
                values
                (
	                RecordIdentifierId,
					Description
                );
END$$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE spInsertIntoStreetDescriptor(
	IN USRN bigint,
	IN STREET_DESCRIPTOR Longtext,
	IN LOCALITY_NAME Longtext,
	IN TOWN_NAME Longtext,
	IN ADMINSTRATIVE_AREA Longtext,
	IN LANGUAGE Longtext,
	IN VersionId int
	)
BEGIN
	insert into StreetDescriptor
				(
					USRN,
					STREET_DESCRIPTOR,
					LOCALITY_NAME,
					TOWN_NAME,
					ADMINSTRATIVE_AREA,
					LANGUAGE,
					VersionId
				)
                values
                (
	                USRN,
					STREET_DESCRIPTOR,
					LOCALITY_NAME,
					TOWN_NAME,
					ADMINSTRATIVE_AREA,
					LANGUAGE,
					VersionId
                );
END$$
DELIMITER ;



DELIMITER $$
CREATE PROCEDURE spInsertIntoStreetRecord(
	IN USRN bigint,
	IN RECORD_TYPE bigint,
	IN SWA_ORG_REF_NAMING bigint,
	IN STATE bigint,
	IN STATE_DATE date,
	IN STREET_SURFACE bigint,
	IN STREET_CLASSIFICATION bigint,
	IN VERSION bigint,
	IN RECORD_ENTRY_DATE date,
	IN LAST_UPDATE_DATE date,
	IN STREET_START_DATE date,
	IN STREET_END_DATE date,
	IN STREET_START_X decimal(12, 2),
	IN STREET_START_Y decimal(12, 2),
	IN STREET_END_X decimal(12, 2),
	IN STREET_END_Y decimal(12, 2),
	IN STREET_TOLERANCE bigint,
	IN VersionId int
	)
BEGIN
	insert into StreetRecord
				(
					USRN,
					RECORD_TYPE,
					SWA_ORG_REF_NAMING,
					STATE,
					STATE_DATE,
					STREET_SURFACE,
					STREET_CLASSIFICATION,
					VERSION,
					RECORD_ENTRY_DATE,
					LAST_UPDATE_DATE,
					STREET_START_DATE,
					STREET_END_DATE,
					STREET_START_X,
					STREET_START_Y,
					STREET_END_X,
					STREET_END_Y,
					STREET_TOLERANCE,
					VersionId
				)
                values
                (
	                USRN,
					RECORD_TYPE,
					SWA_ORG_REF_NAMING,
					STATE,
					STATE_DATE,
					STREET_SURFACE,
					STREET_CLASSIFICATION,
					VERSION,
					RECORD_ENTRY_DATE,
					LAST_UPDATE_DATE,
					STREET_START_DATE,
					STREET_END_DATE,
					STREET_START_X,
					STREET_START_Y,
					STREET_END_X,
					STREET_END_Y,
					STREET_TOLERANCE,
					VersionId
                );
END$$
DELIMITER ;



DELIMITER $$
CREATE PROCEDURE spInsertIntoRecordVersion(
	IN StartDateTime datetime(3),
	IN EndDateTime datetime(3),
	IN SourceFilename Longtext,
	IN Messages Longtext
	)
BEGIN
	insert into Version
				(
				StartDateTime,
				EndDateTime,
				SourceFilename,
				Messages
				)
                values
                (
                StartDateTime,
				EndDateTime,
				SourceFilename,
				Messages
                );
END$$
DELIMITER ;
