package com.placecube.digitalplace.address.dtf.model;

import java.util.HashMap;
import java.util.Map;

public enum DTFChangeType {
	
	DELETE("D"),
	INSERT("I"),
	UPDATE("U");
	
	private static Map<String,  DTFChangeType> changeTypes = new HashMap<>();
	
	static {
		for (DTFChangeType dtfChangeType : DTFChangeType.values()) {
			if (dtfChangeType == DELETE) {
				
				changeTypes.put("D", dtfChangeType);
				
			} else if (dtfChangeType == INSERT) {
				
				changeTypes.put("I", dtfChangeType);
				
			} else if (dtfChangeType == UPDATE) {
				
				changeTypes.put("U", dtfChangeType);
				
			}
		}
	}
	
	private String dtfRecordRepresentation;
	
	DTFChangeType (String dtfRecordRepresentation) {
		this.dtfRecordRepresentation = dtfRecordRepresentation;
	}
	
	public String getDtfRecordRepresentation() {
		return dtfRecordRepresentation;
	}
	
	public static DTFChangeType getChangeTypeFromString(String changeType) {
		return changeTypes.get(changeType.toUpperCase());
	}
	
}