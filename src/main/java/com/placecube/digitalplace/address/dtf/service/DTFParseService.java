package com.placecube.digitalplace.address.dtf.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.Reader;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
import com.placecube.digitalplace.address.dtf.model.DTFChangeType;
import com.placecube.digitalplace.address.dtf.model.DTFDatabaseRecord;
import com.placecube.digitalplace.address.dtf.model.DTFRecord;

public class DTFParseService {
	private static final Logger LOG = LogManager.getLogger(DTFParseService.class);

	private DatabaseService databaseService;

	private List<String> messages = ObjectFactory.newArrayList();

	private Set<String> processedFiles = ObjectFactory.newHashSet();

	private int versionId = 0;

	private DTFParseService(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}

	public static DTFParseService fromDatabaseService(DatabaseService databaseService) {
		return new DTFParseService(databaseService);
	}

	private void addMessage(String message) {
		messages.add(message);
		LOG.info(message);
	}

	public void updateDatabaseFromFile(File file, DTFChangeType changeTypeToProcess) throws Exception {

		setVersionId(file);
		addMessage(String.format("Using file %s", file.getAbsolutePath()));
		addMessage(String.format("Setting version to %d", versionId));

		LOG.info("Processing lines in file.");

		insertLinesFromFile(file, changeTypeToProcess);

		LOG.info("Done." + changeTypeToProcess + ":" + file.getAbsolutePath());

		updateVersionTableMessages();
	}

	private void setVersionId(File file) {
		String fileAbsolutePath = file.getAbsolutePath();
		if (!processedFiles.contains(fileAbsolutePath)) {
			processedFiles.add(fileAbsolutePath);
			String sql = String.format(SqlConstants.ADD_VERSION, ObjectFactory.getTimeNow(), fileAbsolutePath);

			versionId = databaseService.runSqlWithResult(sql);
		}
	}

	private void updateVersionTableMessages() {
		String sql = String.format(SqlConstants.UPDATE_VERSION_MESSAGE, StringUtils.join(messages, ";\r\n"),
				ObjectFactory.getTimeNow(), versionId);

		databaseService.runSql(sql);
		messages.clear();
	}

	private void insertLinesFromFile(File dtfFile, DTFChangeType changeTypeToProcess) throws Exception {
		int counter = 0;

		FileInputStream dataFileInputStream = ObjectFactory.createFileInputStream(dtfFile);
		try (Reader in = ObjectFactory.createInputStreamReader(dataFileInputStream)) {

			Iterable<CSVRecord> dtfCsvRecords = ObjectFactory.createCsvRecordIterator(in);
			
			int batchCounter = 0;
			for (CSVRecord dtfCsvRecord : dtfCsvRecords) {
				DTFRecord dtfRecord = ObjectFactory.createDTFRecord(dtfCsvRecord, versionId);

				if (!dtfRecord.isPermittedChangeTypeAndIdentifier(changeTypeToProcess)) {
					continue;
				}

				DTFDatabaseRecord dtfDatabaseRecord = ObjectFactory.createDTFDatabaseRecord(dtfRecord);

				counter++;
				batchCounter++;

				databaseService.processDtfDatabaseRecord(dtfDatabaseRecord, changeTypeToProcess);

				if (batchCounter == 5000) {
					LOG.info("Added:" + counter);
					batchCounter = 0;
				}
			}

		} catch (Exception e) {
			LOG.error("Error reading in CSV.", e);
		}

		addMessage(String.format("File has %d entries for type %s", counter, changeTypeToProcess));
	}

}
