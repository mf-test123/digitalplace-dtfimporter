package com.placecube.digitalplace.address.dtf.model;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum LpiField {
	
	UPRN(3, Long.class),
	LPI_KEY(4, String.class),
	LANGUAGE(5, String.class),
	LOGICAL_STATUS(6, Long.class),
	START_DATE(7, LocalDate.class),
	END_DATE(8, LocalDate.class),
	ENTRY_DATE(9, LocalDate.class),
	LAST_UPDATE_DATE(10, LocalDate.class),
	SAO_START_NUMBER(11, Long.class),
	SAO_START_SUFFIX(12, String.class),
	SAO_END_NUMBER(13, Long.class),
	SAO_END_SUFFIX(14, String.class),
	SAO_TEXT(15, String.class),
	PAO_START_NUMBER(16, Long.class),
	PAO_START_SUFFIX(17, String.class),
	PAO_END_NUMBER(18, Long.class),
	PAO_END_SUFFIX(19, String.class),
	PAO_TEXT(20, String.class),
	USRN(21, Long.class),
	LEVEL(22, String.class),
	POSTAL_ADDRESS(23, String.class),
	POSTCODE(24, String.class),
	POST_TOWN(25, String.class),
	OFFICIAL_FLAG(26, String.class),
	CUSTODIAN_ONE(27, Long.class),
	CUSTODIAN_TWO(28, Long.class),
	CAN_KEY(29, String.class);
	
	private int columnNumber;
	
	private Class<?> fieldClass;
	
	private static List<LpiField> fieldsInAscOrderByPosition;
	
	static {
		fieldsInAscOrderByPosition = Arrays.asList(LpiField.values())
				.stream()
				.sorted(Comparator.comparingInt(LpiField::getColumnNumber))
				.collect(Collectors.toList());
	}
	
	LpiField(int columnNumber, Class<?> fieldClass) {
		this.columnNumber = columnNumber;
		this.fieldClass = fieldClass;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
	
	public Class<?> getFieldClass() {
		return fieldClass;
	}
	
	public static List<LpiField> getFieldsInPositionOrder() {
		return fieldsInAscOrderByPosition;
	}

}
