package com.placecube.digitalplace.address.dtf.model;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum DTFField {

	RECORD_CHANGE_TYPE(1, String.class), 
	RECORD_IDENTIFIER(0, Integer.class), 
	RECORD_PRO_ORDER(2, Long.class);

	private int columnNumber;

	private Class<?> fieldClass;

	private static List<DTFField> fieldsInAscOrderByPosition;

	static {
		fieldsInAscOrderByPosition = Arrays.asList(DTFField.values()).stream()
				.sorted(Comparator.comparingInt(DTFField::getColumnNumber)).collect(Collectors.toList());
	}

	DTFField(int columnNumber, Class<?> fieldClass) {
		this.columnNumber = columnNumber;
		this.fieldClass = fieldClass;
	}

	public int getColumnNumber() {
		return columnNumber;
	}

	public Class<?> getFieldClass() {
		return fieldClass;
	}

	public static List<DTFField> getFieldsInPositionOrder() {
		return fieldsInAscOrderByPosition;
	}

}
