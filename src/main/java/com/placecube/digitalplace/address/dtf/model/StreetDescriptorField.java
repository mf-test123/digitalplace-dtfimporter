package com.placecube.digitalplace.address.dtf.model;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum StreetDescriptorField {
	
	USRN(3, Long.class),
	DESCRIPTOR(4, String.class),
	LOCALITY(5, String.class),
	TOWN_NAME(6, String.class),
	ADMIN_AREA(7, String.class),
	LANGUAGE(8, String.class);
	
	private int columnNumber;
	
	private Class<?> fieldClass;
	
	private static List<StreetDescriptorField> fieldsInAscOrderByPosition;
	
	static {
		fieldsInAscOrderByPosition = Arrays.asList(StreetDescriptorField.values())
				.stream()
				.sorted(Comparator.comparingInt(StreetDescriptorField::getColumnNumber))
				.collect(Collectors.toList());
	}
	
	StreetDescriptorField(int columnNumber, Class<?> fieldClass) {
		this.columnNumber = columnNumber;
		this.fieldClass = fieldClass;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
	
	public Class<?> getFieldClass() {
		return fieldClass;
	}
	
	public static List<StreetDescriptorField> getFieldsInPositionOrder() {
		return fieldsInAscOrderByPosition;
	}

}
