package com.placecube.digitalplace.address.dtf.program;

import java.time.LocalDateTime;

import com.placecube.digitalplace.address.dtf.service.ActionExecutorService;
import com.placecube.digitalplace.address.dtf.service.ArgumentsService;
import com.placecube.digitalplace.address.dtf.service.MessageService;
import com.placecube.digitalplace.address.dtf.service.ObjectFactory;

public class Program {

	private static ArgumentsService argumentsService;

	private static ActionExecutorService actionExecutorService;

	private static void init(String[] args) throws Exception {

		argumentsService = ObjectFactory.createArgumentsService(args);
		
		actionExecutorService = ObjectFactory.createActionExecutorService(argumentsService);
		
	}

	public static void main(String[] args) {

		MessageService.printMessage("Started at " + LocalDateTime.now());
		try {

			init(args);
			actionExecutorService.executeAction();

		} catch (Exception e) {
			MessageService.printMessage("Error:" + e.getMessage());
			e.printStackTrace();
		}

		MessageService.printMessage("Ended at " + LocalDateTime.now());

	}
}
