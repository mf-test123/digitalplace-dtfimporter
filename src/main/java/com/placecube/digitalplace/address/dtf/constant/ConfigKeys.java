package com.placecube.digitalplace.address.dtf.constant;

public final class ConfigKeys {
	
	public static final String DB_DRIVER_NAME = "db.driver";
	
	public static final String DB_URL = "db.url";
	
	public static final String DB_USERNAME = "db.username";
	
	public static final String DB_PASSWORD = "db.password";
	
	public static final String DTF_SOURCE_FILE_FOLDER = "dtf.source.file.or.folder";
	
	private ConfigKeys() {
		
	}

}
