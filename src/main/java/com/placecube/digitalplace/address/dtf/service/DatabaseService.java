package com.placecube.digitalplace.address.dtf.service;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.placecube.digitalplace.address.dtf.constant.ConfigKeys;
import com.placecube.digitalplace.address.dtf.constant.SqlConstants;
import com.placecube.digitalplace.address.dtf.model.DTFChangeType;
import com.placecube.digitalplace.address.dtf.model.DTFDatabaseRecord;

public class DatabaseService {

	private static final Logger LOG = LogManager.getLogger(DatabaseService.class);
	
	private BasicDataSource basicDataSource = null;
	
	private Properties configProperties;
	
	private void init() {
		String url = configProperties.getProperty(ConfigKeys.DB_URL);
		String driverName = configProperties.getProperty(ConfigKeys.DB_DRIVER_NAME);
		String username = configProperties.getProperty(ConfigKeys.DB_USERNAME);
		String password = configProperties.getProperty(ConfigKeys.DB_PASSWORD);

		basicDataSource = ObjectFactory.createDataSource();
		basicDataSource.setUrl(url);
		basicDataSource.setDriverClassName(driverName);
		basicDataSource.setUsername(username);
		basicDataSource.setPassword(password);
		basicDataSource.setInitialSize(10);
	}
	
	public void setBasicDataSource(BasicDataSource basicDataSource) {
		this.basicDataSource = basicDataSource;
	}
	
	private DatabaseService(Properties configProperties) {
		this.setConfigProperties(configProperties);
		
		init();
	}
	
	public static DatabaseService fromConfigProperties(Properties configProperties) {
		return new DatabaseService(configProperties);
	}
	
	public void setConfigProperties(Properties configProperties) {
		this.configProperties = configProperties;
	}

	public void processDtfDatabaseRecord(DTFDatabaseRecord dtfDatabaseRecord, DTFChangeType dtfChangeType) {
		if (DTFChangeType.INSERT == dtfChangeType) {
			insertDtfDatabaseRecord(dtfDatabaseRecord);
		} else if (DTFChangeType.UPDATE == dtfChangeType) {
			deleteDtfDatabaseRecord(dtfDatabaseRecord);
			insertDtfDatabaseRecord(dtfDatabaseRecord);
		} else if (DTFChangeType.DELETE == dtfChangeType) {
			deleteDtfDatabaseRecord(dtfDatabaseRecord);
		}
	}
	
	public void clearEverything() {
		runSql(SqlConstants.CLEAR_ALL);
	}

	private void insertDtfDatabaseRecord(DTFDatabaseRecord dtfDatabaseRecord) {
		callProcedure(dtfDatabaseRecord.getInsertSql(), dtfDatabaseRecord.getDatabaseInsertParameters());
	}

	private void deleteDtfDatabaseRecord(DTFDatabaseRecord dtfDatabaseRecord) {
		runSql(dtfDatabaseRecord.getDeleteSql());
	}

	private void callProcedure(String spName, List<?> params) {
		
		Connection con = null;
		
		try {
			con = basicDataSource.getConnection();

			callStoredProcedure(con, spName, params);

		} catch (Exception e) {
			LOG.error("Error running stored procedure.", e);
		} finally {
			closeConnection(null, null, con);
		}
		
	}

	public void runSql(String sql) {
		Connection con = null;
		Statement statement = null;
		try {
			con = basicDataSource.getConnection();

			statement = con.createStatement();
			statement.execute(sql);
		} catch (Exception e) {
			LOG.error("Error executing sql:" + sql, e);
		} finally {
			closeConnection(null, statement, con);
		}
	}
	
	public boolean isVersionExists(String fileName) {
		Connection con = null;
		ResultSet rs = null;
		
		String sql = String.format(SqlConstants.GET_MATCHING_FILE, fileName);
		try {
			con = basicDataSource.getConnection();

			Statement statement = con.createStatement();
			rs = statement.executeQuery(sql);
			return rs.next();
		} catch (Exception e) {
			LOG.error("Error executing sql:" + sql, e);
		} finally {
			closeConnection(rs, null, con);
		}
		
		return false;

	}

	public int runSqlWithResult(String sql) {
		Connection con = null;
		ResultSet rs = null;
		Statement statement = null;
		try {
			con = basicDataSource.getConnection();

			statement = con.createStatement();
			statement.execute(sql, Statement.RETURN_GENERATED_KEYS);
			rs = statement.getGeneratedKeys();

			if (rs.next()) {
				return rs.getInt(1);
			}

		} catch (Exception e) {
			LOG.error("Error executing sql:" + sql, e);
		} finally {
			closeConnection(rs, statement, con);
		}

		return 0;
	}

	private void closeConnection(ResultSet rs, Statement stmt, Connection con) {

		if (rs != null) {
			try {
				rs.close();
			} catch (Exception e) {
			}
		}

		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception e) {
			}
		}

		if (con != null) {
			try {
				con.close();
			} catch (Exception e) {
			}
		}
	}

	private void callStoredProcedure(Connection con, String sStoredProcedureName, List<?> params) throws SQLException {
		StringWriter sql = new StringWriter();
		sql.append("{call ");
		sql.append(sStoredProcedureName);
		if (params != null && !params.isEmpty()) {
			sql.append(" (");
			for (int i = 0; i < params.size(); i++) {
				if (i > 0)
					sql.append(",");
				sql.append("?");
			}
			sql.append(")");
		}
		sql.append("}");

		
		CallableStatement statement = null;
		try {
			statement = con.prepareCall(sql.toString());
			statement = (CallableStatement) addParamsToStatement(statement, params);
			statement.execute();
		} catch (Exception e) {
			throw e;
		} finally {
			closeConnection(null, statement, con);
		}
		
	}

	private static PreparedStatement addParamsToStatement(PreparedStatement ps, List<?> paramList) throws SQLException {
		
		for (int i = 0; paramList != null && i < paramList.size(); i++) {
			
			Object param = paramList.get(i);
			
			if (param instanceof String) {
				ps.setString(i + 1, (String) param);
			} else if (param instanceof Integer) {
				ps.setInt(i + 1, ((Integer) param).intValue());
			} else if (param instanceof Long) {
				ps.setLong(i + 1, ((Long) param).longValue());
			} else if (param instanceof LocalDate) {
				LocalDate ld = (LocalDate) param;
				ps.setTimestamp(i + 1, Timestamp.from(ld.atStartOfDay(ZoneOffset.UTC).toInstant()));
			} else if (param instanceof BigDecimal) {
				ps.setBigDecimal(i + 1, (BigDecimal)param);
			} else {
				ps.setObject(i + 1, null);
			}
		}

		return ps;
	}

}