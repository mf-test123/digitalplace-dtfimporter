package com.placecube.digitalplace.address.dtf.model;

import java.util.List;

public interface DTFDatabaseRecord {
	List<Object> getDatabaseInsertParameters();

	String getDeleteSql();
	
	String getInsertSql();
}
