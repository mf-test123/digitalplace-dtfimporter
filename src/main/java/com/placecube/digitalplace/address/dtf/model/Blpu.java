package com.placecube.digitalplace.address.dtf.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;

/*
 * Basic Land and Property Unit - DTF Record Identifier 21
 */
public class Blpu implements DTFDatabaseRecord {

	private DTFRecord dtfRecord;
	
	private Map<BlpuField, Object> fields = new HashMap<>();

	public Blpu(DTFRecord dtfRecord) {
		this.dtfRecord = dtfRecord;
		for (BlpuField blpuField : BlpuField.values()) {
			int columnIndex = blpuField.getColumnNumber();
			fields.put(blpuField, dtfRecord.columnValueToObject(columnIndex, blpuField.getFieldClass()));
		}
	}

	@Override
	public List<Object> getDatabaseInsertParameters() {
		List<Object> params = new ArrayList<>();

		for (BlpuField blpuField : BlpuField.getFieldsInColumnOrder()) {
			params.add(fields.get(blpuField));
		}

		params.add(dtfRecord.getVersionId());

		return params;
	}

	@Override
	public String getDeleteSql() {
		return String.format(SqlConstants.DELETE_BY_UPRN, SqlConstants.TABLE_BLPU,
				dtfRecord.getColumnValue(BlpuField.UPRN.getColumnNumber()));
	}

	@Override
	public String getInsertSql() {
		return SqlConstants.INSERT_BLPU;
	}

}
