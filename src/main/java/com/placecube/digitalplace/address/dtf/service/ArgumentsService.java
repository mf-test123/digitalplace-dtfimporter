package com.placecube.digitalplace.address.dtf.service;

public class ArgumentsService {
	
	public static final String ACTION_CLEAR = "clearall";
	
	public static final String ACTION_LOAD_FILE = "loadfile";
	
	private static final int MINIMUM_ARGUMENT_COUNT = 2;
	
	private static final int PARAM_ACTION_INDEX = 0;
	
	private static final int PARAM_CONFIG_INDEX = 1;
	
	public static int getMinimumArgumentCount() {
		return MINIMUM_ARGUMENT_COUNT;
	}

	public static int getParamActionIndex() {
		return PARAM_ACTION_INDEX;
	}

	public static int getParamConfigIndex() {
		return PARAM_CONFIG_INDEX;
	}
	
	private String[] args;
	
	private ArgumentsService(String[] args) {
		if (args == null) throw new RuntimeException("No arguments were supplied.");
		if (args.length < MINIMUM_ARGUMENT_COUNT) throw new RuntimeException("Not enough arguments were supplied.");
		
		this.args = args;
	}
	
	public static ArgumentsService fromArgs(String[] args) {
		return new ArgumentsService(args);
	}

	public String getConfigFilePath() {
		checkArgExists(PARAM_CONFIG_INDEX, "Configuration file path.");
		return args[PARAM_CONFIG_INDEX];
	}

	public String getAction() {
		checkArgExists(PARAM_ACTION_INDEX, "Action");
		return args[PARAM_ACTION_INDEX];
	}
	
	private void checkArgExists(int argIndex, String argName) {
		if (args.length < argIndex - 1) {
			throwException("Missing argument:" + argName);
		}
	}
	
	private Exception throwException(String message) {
		throw new RuntimeException(message);
	}
}
