package com.placecube.digitalplace.address.dtf.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum StreetRecordField {
	
	USRN(3, Long.class),
	RECORD_TYPE(4, Long.class),
	SWA_ORG_REF_NAMING(5, Long.class),
	STATE(6, Long.class),
	STATE_DATE(7, LocalDate.class),
	STREET_SURFACE(8, Long.class),
	STREET_CLASSIFICATION(9, Long.class),
	VERSION(10, Long.class),
	RECORD_ENTRY_DATE(11, LocalDate.class),
	LAST_UPDATE_DATE(12, LocalDate.class),
	STREET_START_DATE(13, LocalDate.class),
	STREET_END_DATE(14, LocalDate.class),
	STREET_START_X(15, BigDecimal.class),
	STREET_START_Y(16, BigDecimal.class),
	STREET_END_X(17, BigDecimal.class),
	STREET_END_Y(18, BigDecimal.class),
	STREET_TOLERANCE(19, String.class);
	
	private int columnNumber;
	
	private Class<?> fieldClass;
	
	private static List<StreetRecordField> fieldsInAscOrderByPosition;
	
	static {
		fieldsInAscOrderByPosition = Arrays.asList(StreetRecordField.values())
				.stream()
				.sorted(Comparator.comparingInt(StreetRecordField::getColumnNumber))
				.collect(Collectors.toList());
	}
	
	StreetRecordField(int columnNumber, Class<?> fieldClass) {
		this.columnNumber = columnNumber;
		this.fieldClass = fieldClass;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
	
	public Class<?> getFieldClass() {
		return fieldClass;
	}
	
	public static List<StreetRecordField> getFieldsInPositionOrder() {
		return fieldsInAscOrderByPosition;
	}

}
