package com.placecube.digitalplace.address.dtf.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.placecube.digitalplace.address.dtf.constant.SqlConstants;

/*
 * Street Descriptor - DTF Record Identifier 15
 */
public class StreetDescriptor implements DTFDatabaseRecord {

	private DTFRecord dtfRecord;
	
	private Map<StreetDescriptorField, Object> fields = new HashMap<>();
	
	public StreetDescriptor(DTFRecord dtfRecord) {
		this.dtfRecord = dtfRecord;
		for (StreetDescriptorField streetDescriptorField : StreetDescriptorField.values()) {
			int columnIndex = streetDescriptorField.getColumnNumber();
			fields.put(streetDescriptorField,
					dtfRecord.columnValueToObject(columnIndex, streetDescriptorField.getFieldClass()));
		}

	}

	@Override
	public List<Object> getDatabaseInsertParameters() {

		List<Object> params = new ArrayList<>();

		for (StreetDescriptorField streetDescriptorField : StreetDescriptorField.getFieldsInPositionOrder()) {
			params.add(fields.get(streetDescriptorField));
		}

		params.add(dtfRecord.getVersionId());

		return params;
	}

	@Override
	public String getDeleteSql() {
		return String.format(SqlConstants.DELETE_BY_USRN, SqlConstants.TABLE_STREET_DESCRIPTOR,
				dtfRecord.getColumnValue(StreetRecordField.USRN.getColumnNumber()));
	}

	@Override
	public String getInsertSql() {
		return SqlConstants.INSERT_STREET_DESCRIPTOR;
	}

}
