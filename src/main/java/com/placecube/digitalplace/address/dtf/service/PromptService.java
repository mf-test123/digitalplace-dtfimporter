package com.placecube.digitalplace.address.dtf.service;

import java.io.IOException;

public final class PromptService {
	
	private static int Y_ASCII_CODE = 121;
	
	private PromptService() {
		
	}

	public static boolean promtpForConfirmation() throws IOException {
		int confirmClear = System.in.read();
		if (confirmClear == Y_ASCII_CODE) {
			return true;
		} else {
			return false;
		}
	}
}
