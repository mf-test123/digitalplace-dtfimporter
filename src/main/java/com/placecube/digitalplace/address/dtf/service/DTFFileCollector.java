package com.placecube.digitalplace.address.dtf.service;

import static com.placecube.digitalplace.address.dtf.constant.DateFormatConstants.FILENAME_DATE_FORMAT;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class DTFFileCollector {

	private DTFFileCollector() {

	}

	public static List<File> getFilesFromFolder(Path folderPath, String extension) throws IOException {

		List<File> files = new ArrayList<>();

		Files.list(folderPath)
			.filter(path -> path.toString().endsWith(extension))
			.filter(path -> extractDateFromFilename(path.getFileName().toString()) != null)
			.collect(Collectors.toList())
			.forEach(path -> {
				files.add(new File(path.toAbsolutePath().toString()));
			});

		return files;

	}
	
	public static Comparator<File> getDTFFileComparator() {
		return (File file1, File file2) -> {

			Date fileDate1 = extractDateFromFilename(file1.getName());
			Date fileDate2 = extractDateFromFilename(file2.getName());
			
			if (fileDate1 == null) {
				return 1;
			}
			
			if (fileDate2 == null) {
				return -1;
			}

			return fileDate1.compareTo(fileDate2);

		};
	}

	private static Date extractDateFromFilename(String fileName) {
		try {

			SimpleDateFormat fileDateFormatter = new SimpleDateFormat(FILENAME_DATE_FORMAT);

			int dateStartsAt = fileName.indexOf('_');
			if (dateStartsAt > 0 && fileName.length() > (FILENAME_DATE_FORMAT.length() + dateStartsAt)) {
				String dateToParse = fileName.substring(dateStartsAt + 1,
						fileName.length() + 1 - FILENAME_DATE_FORMAT.length());
				Date date = fileDateFormatter.parse(dateToParse);

				return date;
			}

		} catch (ParseException e) {
			// Silently catch
		}

		return null;

	}
}
