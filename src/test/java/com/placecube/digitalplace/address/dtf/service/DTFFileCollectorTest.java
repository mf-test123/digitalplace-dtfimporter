package com.placecube.digitalplace.address.dtf.service;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class DTFFileCollectorTest {
	
	private static final String FILENAME_OLDEST = "5660_20170201_01.csv";
	
	private static final String FILENAME_MIDDLE = "5660_20181222_01.csv";
	
	private static final String FILENAME_NEWEST = "5660_20190329_01.csv";
	
	@Mock
	private DirectoryStream<Path> mockDirectoryStream;
	
	@Mock
	private Path mockPath;
	
	@Mock
	private File mockFileOldest;
	
	@Mock
	private File mockFileMiddle;
	
	@Mock
	private File mockFileNewest;
	
	@Before
	public void setUp() {
		initMocks(this);
	}
	
	@Test
	public void getDTFFileComparator_whenSupplyingUnsortedFileList_thenFilesAreReturnedOldestFirst() throws IOException {
		mockFiles();

		List<File> unsortedFiles = new ArrayList<>();
		unsortedFiles.add(mockFileNewest);
		unsortedFiles.add(mockFileOldest);
		unsortedFiles.add(mockFileMiddle);
		
		List<File> expectedFiles = new ArrayList<>();
		expectedFiles.add(mockFileOldest);
		expectedFiles.add(mockFileMiddle);
		expectedFiles.add(mockFileNewest);
		
		unsortedFiles.sort(DTFFileCollector.getDTFFileComparator());
		
		assertThat(unsortedFiles, IsIterableContainingInOrder.contains(expectedFiles.toArray()));
	}
	
	@Test
	public void getDTFFileComparator_whenSupplyingSortedFileList_thenFilesAreReturnedInSameOrder() throws IOException {
		mockFiles();

		List<File> unsortedFiles = new ArrayList<>();
		unsortedFiles.add(mockFileOldest);
		unsortedFiles.add(mockFileMiddle);
		unsortedFiles.add(mockFileNewest);
		
		List<File> expectedFiles = new ArrayList<>();
		expectedFiles.add(mockFileOldest);
		expectedFiles.add(mockFileMiddle);
		expectedFiles.add(mockFileNewest);
		
		unsortedFiles.sort(DTFFileCollector.getDTFFileComparator());

		assertThat(unsortedFiles, IsIterableContainingInOrder.contains(expectedFiles.toArray()));
	}
	
	private void mockFiles() {
		when(mockFileOldest.getName()).thenReturn(FILENAME_OLDEST);
		when(mockFileMiddle.getName()).thenReturn(FILENAME_MIDDLE);
		when(mockFileNewest.getName()).thenReturn(FILENAME_NEWEST);
	}

}
