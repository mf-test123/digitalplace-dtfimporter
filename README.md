﻿## Summary

This project loads a file in DTF7.3 format into a database. 

DTF7.3 is the Data Transfer Format specification used by local councils to export their address data from their LLPG, in a format matching BS 7666:2006.

The data is loaded into a set of database tables - BLPUs, LPIs and Street Descriptors making it easy to list all the addresses and use sql to query them.

The code also supports DTF delta files, where records are updated and deleted as well as inserted.

## Code Example

The project uses a console gradle project.

It targets MySQL as the database.


## Installation
* Create a blank Sql database
* Run "digitalplace-dtfimporter/src/main/resources/db/schema.sql" to create the schema
* Use "./gradlew clean build distZip" to build the project
* Once built, go to the directory containing the zip file e.g. "digitalplace-dtfimporter/build/distributions/digitalplace-dtfimporter.zip"
* Extract the zip file into a folder and enter the "digitalplace-dtfimporter" folder.  From here you should run the application.
* There is a sample "config.properties" within the jar "digitalplace-dtfimporter.jar".  Extract this and modify accordingly.
* To process DTF files providing an action of "loadfile" - arg 0.  Provide a filename or a folder.  
	If a folder is specified then the folder will be scanned for ".csv" files.  The scan is NOT recursive.
* E.g java -cp "lib/*:digitalplace-dtfimporter.jar" com.placecube.digitalplace.address.dtf.program.Program loadfile "/path/to/config/file/config.properties"
* To wipe out all database data you can execute  
	java -cp "lib/*:digitalplace-dtfimporter.jar"
	com.placecube.digitalplace.address.dtf.program.Program clearall "/path/to/config/file/config.properties"


## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
